package br.edu.uniateneu.bolaows.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.edu.uniateneu.bolaows.model.CampeonatoEntity;

@Repository
public interface CampeonatoRepository extends JpaRepository<CampeonatoEntity, Long> {

}
