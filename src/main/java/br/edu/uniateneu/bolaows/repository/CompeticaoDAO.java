package br.edu.uniateneu.bolaows.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.edu.uniateneu.bolaows.model.CampeonatoEntity;
import br.edu.uniateneu.bolaows.model.EnumTipoCompeticao;

public class CompeticaoDAO {

public Connection connection = null;
	
public List<CampeonatoEntity> listAll() throws SQLException{
	
	PreparedStatement stmt = (PreparedStatement) this.connection.prepareStatement("select * from tb_campeonato");
	ResultSet rs = (ResultSet) stmt.executeQuery();

	//retorna Array list do tipo Contato
	ArrayList campeonatoList = new ArrayList();

	while (rs.next())
	{
	//Obtenho os dados do banco através do rs.getString, e atraves do objeto contato do Tipo Contato eu seto os seus parametros/

	CampeonatoEntity campeonato = new CampeonatoEntity();
	campeonato.setId(rs.getLong("cd_competicao"));
	campeonato.setAno(rs.getInt("nr_ano"));
	campeonato.setNome(rs.getString("nm_competicao"));
	//campeonato.setTipo(EnumTipoCompeticao(rs.getString("tp_competicao")));

	campeonatoList.add(campeonato);
	}
	rs.close();
	stmt.close();
	return campeonatoList;
} 
	
	
}
